import clasesSaati as SAATI

orderUFV = SAATI.Order("1", "UFV", "wool", 1, 10, 1, 2, 1, 10)
Sheet1 = SAATI.FabricSheet("1", "wool", 100, 100, "department1", True)
Piece1 = SAATI.StockPiece("1", "normal", 10, 10, 9, 7)
Sheet1.maps["Initial"].addPiece(Piece1)
orderUFV.addPiece(Piece1)

print(f'The area of the order is {orderUFV.areaOfOrder()}.')


orderRoberto = SAATI.Order("2", "Roberto", "leather", 1, 15, 1, 2, 1, 12)
Sheet1 = SAATI.FabricSheet("2", "leather", 100, 100, "department2", True)
Piece1 = SAATI.StockPiece("2", "normal", 15, 12, 1, 1)
Sheet1.maps["Initial"].addPiece(Piece1)
orderRoberto.addPiece(Piece1)

print(f'The area of the order is {orderRoberto.areaOfOrder()}.')


orderSergio = SAATI.Order("3", "Sergio", "alcantara", 1, 17, 1, 2, 1, 13)
Sheet1 = SAATI.FabricSheet("3", "alcantara", 100, 100, "department3", True)
Piece1 = SAATI.StockPiece("3", "normal", 13, 11, 7, 5)
Sheet1.maps["Initial"].addPiece(Piece1)
orderSergio.addPiece(Piece1)

print(f'The area of the order is {orderSergio.areaOfOrder()}.')