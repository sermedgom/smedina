"""
Sergio Medina Gomez
Programación II
"""



class Order:

    def __init__(self, id_order, customer, raw_material, min_quality_level, min_height, max_height, min_quantity,
                 max_quantity, min_cut_length, max_cut_length):
        self.id_order = id_order
        self.customer = customer
        self.raw_material = raw_material
        self.min_quality_level = min_quality_level
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_length = min_cut_length
        self.max_cut_length = max_cut_length

    def get_assigned_quantity(self) -> float:
        return sum([stockpieces.get_area() for stockpieces in self.get_assigned_stockpieces])

    def calc_delay(self) -> float:
        raise NotImplementedError


class StockPiece:

    def __init__(self, id_stock_piece, height, length, quality_level, position_x, position_y, id_father_stock_piece):

        self.id_stock_piece = id_stock_piece
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.position_x = position_x
        self.position_y = position_y
        self.id_father_stock_piece = id_father_stock_piece
        self.orders = {}

    def get_area(self) -> float:
        return self.height * self.length

    def checkresidual(self) -> float:
        raise NotImplementedError

    def assign_order(self, order:'orders'):
        for orders in order:
            if (order.min_height < self.height < order.max_height and self.quality_level > order.min_quality_level):
                order = orders.id_order
                return self.orders[order.id_order]


class Map:
    def __init__(self, maptype, real, maparea):
        self.maparea = maparea
        self.maptype = maptype
        self.real = real
        self.assigned_stockpieces = set([])

    def get_assigned_quantity(self) -> float:
        return sum([stockpieces.get_area() for stockpieces in self.get_assigned_stockpieces])

    def calc_residual_area(self) -> float:
        return self.maparea - (self.height * self.length)

    def add_stock_piece(self, stockpieces: 'assigned_stockpieces'):
        stockpiece_assigned = StockPiece.id_stock_piece
        self.assigned_stockpieces.add(stockpiece_assigned)
        return stockpiece_assigned

    def delete_stock_piece(idStockPiece: int):
        raise NotImplementedError


class FabricSheet:

    def __init__(self, id_fabric_sheet, raw_material, height, length, department, weaving_forecast):
        self.id_fabric_sheet = id_fabric_sheet
        self.raw_material = raw_material
        self.height = height
        self.length = length
        self.department = department
        self.weaving_forecast = weaving_forecast
        self.mapFinal = None
        self.mapFinal = None
        self.mapFinal = None

    def select_map(mapType: 'string'):
        return mapType

    def add_map(map: Map):
        return map

    def maps_inicialization(self):
        self.mapFinal = Map("Final", True)
        self.mapIntermediate = Map("Intermediate", True)
        self.mapInitial = Map("Initial", True)
        return self.mapInitial, self.mapIntermediate, self.mapFinal
