import sqlite3
from email_validator import validate_email, EmailNotValidError

conn = sqlite3.connect('users.db')

c = conn.cursor()

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS agendaContactos (id not null primary
key, name, surname, email, phone_1, phone_2, age)''')

# Insert a row of data
"""c.execute(
    "INSERT INTO agendaContactos VALUES (123, 'peter', 'gonzalez', "
    "'petergonzales@gmail.com ', '+673234269', '+673234269', 60)")
c.execute(
    "INSERT INTO agendaContactos VALUES (1234, 'juan', 'gil', "
    "'juangil@gmail.com ', '+654829382','+654829382', 21)")
c.execute(
    "INSERT INTO agendaContactos VALUES (12356, 'alberto', 'roca', "
    "'albertoroca@gmail.com ', '+672926740','+672926740', 15)")
c.execute(
    "INSERT INTO agendaContactos VALUES (123567, 'pepe', 'domingo', "
    "'pepedomingo@gmail.com ', '+611900368', '+611900368', 50)")
c.execute(
    "INSERT INTO agendaContactos VALUES (1235678, 'sech', 'ete', "
    "'etesech@gmail.com ', '+624398010','+624398010', 22)")"""


c.execute("SELECT * FROM agendaContactos")
print(c.fetchall())

"""
Sergio Medina Gomez
Programación II
"""

"this part of code depending on the situation, sets or gets from the database "
"whats needed "
"get--> gets the value attribute from the data base from whom the id match"
"set--> updates the value attribute from the data base from whom the id match"


class validarEmail:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance, owner=None):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):
        try:
            # Validate.
            valid = validate_email(value)
            value = valid.email
            conn.execute(self.store, [value, instance.key])
            conn.commit()

        except EmailNotValidError:
            raise ValueError('NOT VALID EMAIL')


class validatePhone:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance, owner=None):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):

        try:
            primerValor = value[:0]
            numValidos = "0123456789"
            for i in value:
                if primerValor == '+' and (
                        i in numValidos) == True and 10 <= len(value) <= 12:
                    conn.execute(self.store, [value, instance.key])
                    conn.commit()

        except ValueError:
            raise ValueError('NOT VALID PHONE')


class validateAge:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance, owner=None):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):

        try:
            if 0 < value < 120:
                conn.execute(self.store, [value, instance.key])
                conn.commit()

        except ValueError:
            raise ValueError('NOT VALID AGE')


class Field:

    def _set_name_(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def _get_(self, instance):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def _set_(self, instance, value):
        conn.execute(self.store, [value, instance.key])
        conn.commit()


"the class user where the instance will be created"


class user:
    table = 'agendaContactos'
    key = 'id'
    name = Field()
    surname = Field()
    email = validarEmail()
    phone_1 = validatePhone()
    phone_2 = validatePhone()
    age = validateAge()

    def __init__(self, key):
        self.key = key


'''------------------------------------------'''
peter = user(123)
try:
    assert peter.age == 60
except AssertionError:
    print('edad erronea')

try:
    peter.age = 15
except ValueError:
    print('edad erronea')

try:
    assert peter.age == 15
except AssertionError:
    print('edad erronea')

try:
    peter.age = 150
except ValueError:
    print('edad erronea')


'''------------------------------------------'''
pepe = user(123567)
try:
    assert pepe.phone_1 == '+611900368'
except AssertionError:
    print('numero erroneo')

try:
    pepe.phone_1 = '+611246368'
except ValueError:
    print('numero erroneo')

try:
    assert pepe.phone_1 == '+611246368'
except AssertionError:
    print('numero erroneo')

try:
    pepe.phone_1 = '+624273010624273010'
except ValueError:
    print('numero erroneo')


'''------------------------------------------'''
juan = user(1234)
try:
    assert juan.email == 'juangil@gmail.com'
except AssertionError:
    print('email erroneo')

try:
    juan.email = 'juangil23647@gmail.com'
except ValueError:
    print('email erroneo')

try:
    assert juan.email == 'juangil23647@gmail.com'
except AssertionError:
    print('email erroneo')

try:
    juan.email = 'juangil23647gmail.com '
except ValueError:
    print('email erroneo')



# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
