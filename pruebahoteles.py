import numpy as np
from itertools import repeat
import json

dict = {70: 40,
        80: 34,
        90: 30,
        100: 27,
        110: 24,
        120: 22,
        140: 18,
        160: 15}


N = 50000


def revenues_F(nRooms: int, demand: float, price: int) -> float:
    """the concept is to calculate the revenues going on reverse starting
    from friday """
    revenue = price * min(demand, nRooms)
    return revenue

def revenues_T(nRooms1: int, demand: float, price: int) -> float:
    """this function must be the same with one slight difference, we have to
    take into acount the rooms allready sold"""
    revenue = price * min(demand, nRooms1)
    return revenue


def strategy_F(nRooms: int):

    profit = []

    for i in dict.keys():
        # Expected revenue
        """first we calculate the demand with a poisson distribution given a 
        price and the number of obs """
        demand = np.maximum(np.random.poisson(lam=i, size=N), 0)
        revenues_Friday = map(revenues_F, repeat(nRooms, N), demand,
                              repeat(i, N))
        """then we store the profits into an array"""
        profit[i] = sum(revenues_Friday)/N

        """profit has to be positive"""
        if profit[i] > 0:
            return profit[i]

def strategy_T(nRooms1: int):
    """this function is for the next day. Same code, but using the revenues_T (thursday)"""

    profit = []
    for i in dict.keys():
        # Expected revenue
        """first we calculate the demand with a poisson distribution given a 
        price and the number of obs """
        demand = np.maximum(np.random.poisson(lam=i, size=N), 0)
        revenues_Friday = map(revenues_T(), repeat(nRooms1, N), demand,
                              repeat(i, N))
        """then we store the profits into an array"""
        profit[i] = sum(revenues_Friday)/N

        """profit has to be positive"""
        if profit[i] > 0:
            return profit[i]

if __name__ == '__main__':
    sim_results = strategy_F()
    sim_results1 = strategy_T()

    """this next lines create the threads and will repeat 70 times"""
    import multiprocessing as mp
    with mp.Pool(4) as p:
        x = p.map(sim_results, range(0, 71))
    with mp.Pool(4) as p:
        x = p.map(sim_results1, range(0, 71))

    """printing the results of each strategy"""
    best_q = max(sim_results, key=sim_results.get)
    print(best_q)
    best_q1 = max(sim_results, key=sim_results.get)
    print(best_q1)

    """creat a json file for the simulation"""
    with open('newsvendor.json', 'w') as f:
        json.dump(sim_results, f, sort_keys=True, indent=4)